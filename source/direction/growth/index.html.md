---
layout: markdown_page
title: Product Vision - Growth
---

## On this page
{:.no_toc}

- TOC
{:toc}

This page will contain the product vision for accelerating adoption and growth of GitLab and its features. If you'd like to discuss the upcoming vision
directly with the product leader for Growth, feel free to reach out to Eric
Brinkman via [e-mail](mailto:ebrinkman@gitlab.com) or [Twitter](https://twitter.com/ericbrinkman).
We are also hiring for a [Product Director](https://boards.greenhouse.io/gitlab/jobs/4202773002) and several [Senior Product Managers](https://boards.greenhouse.io/gitlab/jobs/4206239002)
for our Growth groups. Please visit our [jobs page](https://jobs.gitlab.com) to find out
more and apply.

## Overview, Philosophy, and Strategy

You can find our approach to the growth stage, including our philosophy, strategy, and team structure in our [Growth handbook page](../../handbook/product/growth/).

## Growth Priorities

One of the first priorities of Growth is making sure we have enough data to do
regression monitoring. At any point in time, we should be able to answer: Are we
making anything worse?

We'll start by generating dashboards for the primary metrics above and other [KPIs for Product](/handbook/ceo/kpis/#vp-of-product).

By analysing the key metrics, which are really part of a full [funnel analysis](#funnel),
we can identity hotspots to tackle first. Then proceed in a scientific fashion
with hypotheses and experimentation.
