---
layout: markdown_page
title: "Manage - Access Group"
---

- TOC
{:toc}

## Access

### Introduction and how you can help

Thanks for visiting this overview of the Access group’s product vision. The Access group is 1 of 2 groups that compose the Manage stage. You can read more about Manage and how it fits into the GitLab product here.

This page is a work in progress, and everyone can contribute:
* Please comment and contribute in the linked issues and epics on this category page. Contributing your feedback directly on GitLab.com is the best way to contribute to our shared vision.
* Please also share feedback with the PM for this stage (Jeremy Watson) via email, Twitter, or on a video call. If you’re one of the GitLab administrators described in the Overview below, we’d especially love to hear from you and how we can make your job easier.

Onward!

### Overview

One of the pillars for the Manage stage of the DevOps lifecycle is **helping organizations prosper with configuration and analytics that enables them to work more efficiently**. As part of Manage, the Access group primarily focuses on the **configuration** part of this mission.

Access is here to serve the GitLab administrator. These are privileged users with the keys to the kingdom; they may range from a CEO/CTO at a smaller organization to an IT manager at a larger company - in any size instance, they’re one of the primary users responsible for setting up, configuring, and maintaining an instance. They’re also busy, frustrated with lengthy configuration steps and tedious workarounds, and keen to automate away tasks they find themselves doing frequently.

As GitLab instances scale, we typically see these problems grow in turn. Not only do the number of members increase, but so do the number of requirements from the business on how they’d like their instance to behave. As native configuration begins to fall short, administrators may fill the gap with their own solutions - whether it’s nginx to manage ingress/egress, or a custom cronjob to periodically check user activity - which demand maintenance and upkeep of their own.

Access’s mission is to solve for these needs directly in GitLab. Onboarding shouldn’t be a tedious task; our supported authentication and identity management should be able to support your enterprise’s existing solution. Security and compliance should be configurable and monitorable. 

Ultimately, we want to natively support the configuration and guardrails an administrator needs to work efficiently - and get a good night’s sleep.

### Categories

Access's relevant categories center around this focus on configuration and access:

| Category | Description |
| ------ | ------ |
| Governance | Ongoing governance of how GitLab is used. Covers audit management, workflow policies, and traceability. |
| [Authentication and authorization](https://about.gitlab.com/direction/manage/access/auth/) | SAML, LDAP, OAuth, and more. | 
| [Groups](https://about.gitlab.com/direction/manage/access/groups/) | Creating and managing groups and subgroups. | 
| [Users](https://about.gitlab.com/direction/manage/access/users/) | Nearly anything related to users in GitLab: how they're managed, our permissions model, user profiles, and user registration. | 
| [Administration](https://about.gitlab.com/direction/manage/access/administration/) | Admin panel and managing abuse in GitLab. | 

### Themes

#### Working at enterprise scale

Whether you’re an “enterprise” or not, GitLab should be maintainable at scale. As described above, Access seeks to find areas where large instances find themselves doing things that don’t scale - whether or not that’s onboarding new employees by hand or managing inactive users. 

#### Security and compliance

The task of managing risk is always on. GitLab should help administrators manage the risk of internal and external threats with a strong, configurable permissions model and with full audit capabilities when further investigation is required. 

### What’s next and why

#### Current (what we're focusing on now)

Details to come.

#### Next (3-4 releases)

Details to come.

#### Future (5-8 releases)

Details to come.
