---
layout: markdown_page
title: "Stage Strategy - Ecosystem"
---

- TOC
{:toc}

## Overview

Ecosystem's mission is to foster GitLab as not just an individual product, but as a platform. Our [ambitious product vision](https://about.gitlab.com/direction/product-vision/) aspires to expand the breadth of the GitLab application well beyond what it is today, but will only be achievable in partnership with other services. While we'll always seek to develop native capabilities inside GitLab that extend our core value proposition, there are often significant advantages associated with integrating other technologies into the GitLab product. Our goal is to make this process seamless from both the end-user and developer perspectives, allowing GitLab users to unlock value in and augment their particular DevOps toolchain in ways that work best for their business.

Today, there are several ways to integrate into GitLab, which can be found on our [partner integration page](https://about.gitlab.com/partners/integrate/). As we grow, we want to extend the ability to create those integrations through a best-in-class set of SDKs, and a robust marketplace that allows users to implement those integrations with ease.

## Categories

### Native Integrations

Native Integrations are first-class features that are developed and maintained as part of the GitLab product that bring the most requested features and services from outside of GitLab to our customers. These integrations offer our customers a seamless experience between the two services, and can range from lightweight features like Slack notifications for GitLab Projects, to deep and complex integrations between Atlassian JIRA that connect a wide variety of functionality from those services directly in to the GitLab product.

[Current Integrations](https://docs.gitlab.com/ee/integration/)

### SDKs & Integration APIs

The GitLab SDK offers other, non-partner companies or individuals to develop their own integrations with and extensions of the GitLab product. The SDK offers a robust set of tools developers need, including the GitLab APIs, native client libraries in an array of popular languages, a CLI utility for interactive and scriptable integration with GitLab, and a complete set of documentation that covers all these tools and offers example code and reference applications for education.

### Marketplace

The GitLab Marketplace offers a place where customers seeking integrations and extensions can search through existing integrations across all three of these categories. It serves as a canonical home for the listing of these integrations and simplifies the installation in to both GitLab.com and self-hosted environments. This will give GitLab users the opportunity to quickly and seamlessly extend GitLab products to customize them to fit their business, without having to touch the command line.

## Themes

### Freedom of choice

We firmly believe that a [single application](https://about.gitlab.com/handbook/product/single-application/) can successfully serve all the needs of the entire DevOps lifecycle. However, we understand that there are a myriad of reasons that you may have specific and particular needs that require you to use a specific tool in your toolchain. With this in mind, we believe that you should have the **freedom to choose your tools wisely** and use what makes the most sense for your business&mdash;and we will support that freedom as best we can.

### Ease-of-use

[We always strive to build solutions that are easy to use, but without oversimplifying the product.](https://about.gitlab.com/handbook/engineering/ux/#easy-ux) We want to offer developers and integrators all the *power and flexibility they need*, and do so in a way that's *productive, minimal, and human*. To achieve this, we offer [simple solutions](https://about.gitlab.com/handbook/product/#convention-over-configuration), provide [excellent documentation](https://docs.gitlab.com/), and value [convention over configuration](https://about.gitlab.com/handbook/product/#convention-over-configuration).

### Flexibility and Extensibility

We can’t imagine every possible use-case, nor can we afford to support the development of every possible integration. However, with our products, we should allow for infinite flexibility and extensibility through our APIs and SDKs, allowing our users to create the experience that they need. Our tools should support users wanting to create a simple extension of functionality, to a robust integration with another product, or even the ability to build a [wholly new product](https://www.penflip.com/) on its own, leveraging GitLab as an underlying platform.

## Challenges

GitLab currently [integrates with others](https://docs.gitlab.com/ee/integration/), but we aspire to allow others to integrate deeper into GitLab and with exceptional ease. Integration partners like [Percy](https://docs.percy.io/v1/docs/gitlab) seek to build with GitLab, but struggle against requirements like [needing a bot user](https://docs.percy.io/v1/docs/gitlab#section-create-a-gitlab-bot-user) and a [limited ability to scope](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#limiting-scopes-of-a-personal-access-token) token access.

Thinking more broadly than specific limitations, the Ecosystem group will need to consider the following:

* Which partners should we work with on developing our ecosystem? What does success look like for these partnerships, and how can we learn along the way?
* How do we design and prioritize our integration APIs? Which areas of the GitLab product do we prioritize, and for which partners?
* How do we measure success for our partners, users, and GitLab? How do we align incentives between all three, including our community contributors?
* How will we make GitLab integration easy for developers? How do we evangelize our ecosystem?
* Can we consider a strategy that serves both our base of self-managed instances as well as GitLab.com?

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking to
get involved with features in the Ecosystem area, [you can find open issues here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Ecosystem).

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## Requesting Integrations

If there's an integration that you'd like to see GitLab offer, please [submit an issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new?issue%5Btitle%5D=New%20GitLab%20Integration%20with) with the label `Ecosystem`, or contact [Patrick Deuley](mailto:pdeuley@gitlab.com), Sr. Product Manager, Ecosystem.

## Influences

We're inspired by other companies with rich, developer-friendly ecosystems like [Salesforce](https://developer.salesforce.com/), [Shopify](https://help.shopify.com/en/api/getting-started), [Twilio](https://www.twilio.com/docs/), [Stripe](https://stripe.com/docs/development), [GitHub](https://github.com/marketplace), and their [APIs](https://developer.github.com/v3/checks/).

A large part of the success of these companies comes from their enthusiasm around enabling developers to integrate, extend, and interact with their services in new and novel ways, creating a spirit of [collaboration](https://about.gitlab.com/handbook/values/#collaboration) and [diversity](https://about.gitlab.com/handbook/values/#collaboration) that simply can't exist any other way.