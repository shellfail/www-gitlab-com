---
layout: job_family_page
title: "Digital Marketing Programs Manager"
---

## Intro

You might describe yourself as analytical, creative, organized, and diplomatic. You have experience driving web traffic to build revenue by deploying a variety of crafts that span marketing, content, and product management. You enjoy setting up conversion rate optimization experiments to continually improve the results of a prospective customers journey, and sharing results and insights with a broad group of stakeholders throughout the company. You also enjoy employing a number of marketing tactics to attract a relevant audience to a marketing site.

## Responsibilities

- Ensure all online marketing enhances our brand with developers, IT ops practitioners, and IT leaders. Be an expert on our audiences and their preferences.
- Be an expert on our customer lifecycle, and ways in which customer needs are evolving/how our product meets them.
- Work with product team to implement marketing strategies that span into trial/product experience, and impact both customer conversion and future retention/upsell.
- Grow inbound demand tracked through GitLab’s marketing site by optimizing the customer journey:
  - Improve website conversion focused on enterprise trial requests, sales contact requests, and live chat engagement.
  - Improve conversion from trial request to download of GitLab.
  - Improve conversion from trial request to sales accepted opportunity.
- Grow trial conversion to paying customer by optimizing the customer journey:
  - Ensure trial feature usage is understood and compared to high-value customer usage
  - Improve adoption of features that prove most valuable in conversion
  - Optimize onboarding experience for engagement & activation
- Develop and execute on strategies to improve free user to paid plan conversion.
- Drive innovation and experimentation on the marketing site & trial experience to improve inbound demand creation & conversion.
- Oversee our paid social and account based advertising programs, maximizing for return on marketing spend.
- Analyze and report data across multiple channels to monitor marketing site growth.

## Specialties and levels

### Search Engine Marketing (or SEO)

The ideal search engine marketer for GitLab understands that large portions of our prospective customers prefer to research a topic on their own terms, and can apply their skill at SEO and to ensure that GitLab comes up when and where it would be relevant to someone's research. We also value meticulous tracking and analysis skills and a deep understanding of Web/marketing Operations with a focus on continual improvement.


#### Search Engine Marketing Associate:

- Conduct keyword research and analysis and identify SEO opportunities.
- Coordinate with content and product marketing teams to identity, optimize, and promote content for targeted keywords or phrases in order to increase traffic to our website - from organic, paid, and social marketing and sponsorships.
- Work with extended team to identify and resolve SEO blockers.
- Assist with building and improving digital marketing campaigns.
- Monitor market trends and competitors to determine marketing opportunities and recommend and take actions to improve visibility of site.
- Analyze and report data across multiple channels to monitor marketing site growth.

##### Requirements
- 1-3 years experience in a search marketing role.
- Experience with Google Analytics, Google Console, and at least one SEO tool.
- Intermediate knowledge of keyword research and the behaviors of search engines.
- Proven ability to work on multiple projects at a time.
- Some experience with technical SEO, SEM, online advertising, web conversion rate optimization to improve lead generation, sales pipeline, and revenue.


#### Search Engine Marketing Manager:
- Provide recommendations for technical SEO improvements to the site.
- Implement our SEO strategy together with our marketing team and digital marketing agency.
- Grow traffic to about.gitlab.com through paid and organic search marketing programs.
- Work with marketing and sales ops to evaluate and implement adtech and martech.
- Work with our content team to improve quality scores of landing pages for paid and organic search.
- Work with our content team to ensure the content we create on the marketing site ranks for our priority search terms.
- Report on marketing site growth from SEO and paid search.
- Ensure web pages are structured and coded in a way to enable consistent and accurate tracking.							
- Work across teams to collaborate on standardized marketing reporting and dashboards, and present performance to the marketing department as well as stakeholders throughout the company.		
- Troubleshoot and formulate solutions to issues with SEO and site lead flow.
- Analyze and report data across multiple channels to monitor marketing site growth.

##### Requirements
- 3+ years in Search Engine Marketing role
- 1-3 years of enterprise software marketing experience.
- 3+ years experience Google Analytics (or related tool), Google Console and experience with at least one SEO tool.
- Technical/industry experience focused on SEO, SEM, online advertising, and/or web conversion rate optimization to improve lead generation, sales pipeline, and revenue.
- Analytics and reporting experience


#### Senior Search Engine Marketing Manager

- Define our SEO strategy together with marketing team and digital marketing agency.
- Build end-to-send SEO programs with recommended keywords, content, and implementation plans and coordinate across teams on execution.
- Improve closed-loop reporting and analysis capabilities for site and SEO.

##### Requirements
- 5+ years in Search Engine Marketing role
- 5+ years experience Google Analytics (or related tool), Google Console and expert-level SEO tool experience.
- 3+ years of enterprise software marketing experience.
- Technical/industry experience focused on SEO, SEM, online advertising, and/or web conversion rate optimization to improve lead generation, sales pipeline, and revenue.
- Advanced analytics and reporting experience including advanced knowledge of Excel, Tableau, SQL, and/or similar.

### Digital Marketing
The ideal digital marketer for GitLab will understand the larger opportunity across marketing channels and will have experience in a wide range of paid marketing tactics as well as a solid background in SEO and the ability to make decisions about the best marketing channels to reach our audience.  They will also be able to use data and research to make marketing decisions and run conversion rate optimization experiments.

#### Digital Marketing Manager
- Assist with building and improving paid search and social campaigns.
- Assist in growing traffic to about.gitlab.com through paid search and social marketing programs.
- Work with marketing and sales ops to evaluate and implement adtech and martech.
- Work with our content team to ensure the content we create on the marketing site is promoted in paid and social marketing sites and sponsorships.
- Monitor market trends and competitors to determine marketing opportunities and recommend and take actions to improve visibility of site.
- Grow trial conversion to paying customer by optimizing the customer journey							
- Develop and execute on strategies to improve free user to paid plan conversion.							

##### Requirements
- 3+ years in Digital Marketing role
- 2+ years experience Google Analytics (or related tool) and Google Adwords and expert-level SEO/SEM tool experience.
- 1-3 years of enterprise software marketing experience.
- 1-3 years paid social media marketing experience
- Technical/industry experience focused on SEO, SEM, online advertising, and/or web conversion rate optimization to improve lead generation, sales pipeline, and revenue.
- Advanced analytics and reporting experience including advanced knowledge of Excel, Tableau, SQL, and/or similar.

####  Senior Digital Marketing Manager
- Define our overall digital strategy together with marketing team and digital marketing agency.
- Manage our paid sponsorship opportunity and budget.
- Develop and manage our paid social and search opportunity and budget.
- Improve closed-loop reporting and analysis capabilities for paid search, social, sponsorships, ABM, and SEO.
- Drive innovation and experimentation on the marketing site & trial experience to improve inbound demand creation & conversion.							

##### Requirements
- 5+ years in Digital Marketing role
- 5+ years experience Google Analytics (or related tool) and Google Adwords and expert-level SEO/SEM tool experience.
- 3+ years of enterprise software marketing experience.
- 3+ years paid social media marketing experience
- Technical/industry experience focused on SEO, SEM, online advertising, and/or web conversion rate optimization to improve lead generation, sales pipeline, and revenue.
- Advanced analytics and reporting experience including advanced knowledge of Excel, Tableau, SQL, and/or similar.

### Email Marketing

####  Email Marketing Manager
- Work with Content, Field, Product Marketing and extended teams and coordinate with Marketing Program Managers to build and manage email campaigns.
- Review and approve all marketing emails.
- Ensure all email campaigns comply with current email best practice in the industry.
- Run Conversion Rate Optimization (CRO) program with A/B testing and gathering and analyzing data, formulate hypotheses, draft copy, and report on performance.
- Gather data and reporting on patterns in email marketing program to drive qualified traffic to website for specific campaigns, date ranges, or email types
- Ensure all email campaigns comply with current email best practice in the industry.
- Evaluate emails against key metrics including CTR, open rates, engagement rates and revenue, and provide optimization recommendations.
Develop dashboards and reports that provide insight and visualization into email performance relative to KPIs, projections, and comparative historical performance.

##### Requirements
- 1-3 years in Digital Marketing role
- Superior oral and written communication skills
- Excellent written and verbal communication skills; able to communicate complex information clearly and quickly
- Highly organized and detail-oriented – you can execute complex tasks flawlessly and with minimal oversight.
- Hands-on experience with email marketing
- Ability to understand and produce insights from email performance data, then use these insights to conceptualize and execute projects that deliver results
- Familiarity with Google Analytics and Google Data Studio
- Familiarity with email best practices, industry trends, CAN-SPAM, CASL and GDPR


## Manager, Digital Marketing Programs

The Manager of Digital Marketing Programs for GitLab should have a background and hands-on experience in all areas of marketing: SEO, SEM, Paid and Organic Social, ABM, Paid sponsorships, and Conversion Rate Optimization. They will have experience managing marketing teams and working with marketing agencies. They should also be able to maintain a budget, use analytics tools, CRM, MAT, and other marketing tools, and have a data-driven approach to marketing. Experience in content marketing and web site maintenance are also helpful in this role.

### Responsibilities
- Administer the Digital Marketing Programs marketing budget and ensure marketing costs are tracked correctly.							
- Manage, build, and lead a strong team by coaching and developing existing members and closing talent gaps where needed through acquisition of new team members.	
- Build and implement Digital Marketing Programs strategy—understand key partnerships within organization to drive full funnel of customer acquisition, retention, and upsell.	
- Define, manage, and implement our overall digital strategy together with marketing team and digital marketing agency.						
- Create strategies for conversion rate optimization (CRO) projects.
- Lead effort to create standardized marketing reporting and dashboards, presenting performance to the marketing department as well as stakeholders throughout the company.		
- Oversee our paid search, social, sponsorships, and account based advertising programs, maximizing for return on marketing spend.
- [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

##### Requirements
- 10+ years in a Digital Marketing role
- 5+ years of enterprise software marketing experience.
- Experience managing a marketing team.
- In-depth industry experience and knowledge in at least one digital marketing or marketing programs specialty.
- Strong technical marketing and advanced digital analytics skills


