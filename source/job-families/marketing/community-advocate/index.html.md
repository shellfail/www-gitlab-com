---
layout: job_family_page
title: "Community Advocate"
---

## Role

As a GitLab Community Advocate, you will be a member of the Community Advocacy team within Developer Relations with a goal of responding to every question about GitLab asked online. You will help to create process and documentation around the way this team interacts with the community as well as help make it easier to provide feedback into GitLab.

## Responsibilities

- Respond to the GitLab community across the following channels in a timely manner: Twitter, Disqus, Facebook, StackOverflow, Reddit, Quora, Google alerts, HackerNews, comments on news stories in the media, [forum.gitlab.com](https://forum.gitlab.com), and more.
- Engage with the developer community in a way that is direct but friendly and authentic. Be able to carry the tone of the GitLab brand while also giving the proper answers or direction to members of the community.
- Help update the [social media guidelines](/handbook/marketing/social-media-guidelines/) and GitLab voice as new situations arise.
- Work with leadership to find a way to track and measure response time across all channels with the ideal response time being under 1 hour for all channels.
- Explore different tools from Zendesk to Mentions to find a way to track all mentions of GitLab across the internet.
- Don’t be afraid of animated gifs and well-placed humor! We are not robots.
- Work within the GitLab process to help users report bugs, make feature requests, contact support, and provide feedback to the product.

## Requirements

- Have an understanding of Git, GitLab, and modern development practices.
- A broad knowledge of the application development ecosystem.
- Be a good conversation partner for experienced developers.
- Know multiple programming languages, Ruby on Rails experience is a plus
- Excellent written and spoken English.
- Accurate, nuanced, direct, and kind messaging.
- Being able to work independent and respond quickly.
- Able to articulate the GitLab mission, values, and vision.
- Understand the difference between different online developer communities
- You LOVE working with the GitLab community.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Developer that maintains contributors.
* A 45 minute interview with our Field Marketing Manager
* A 45 minute interview with our Support Lead
* A 45 minute interview with our Chief Marketing Officer
* Finally, our CEO may choose to conduct a final interview
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Relevant links

- [Marketing Handbook](/handbook/marketing)
- [Developer Advocacy Handbook](/handbook/marketing/developer-relations/developer-advocacy/)
