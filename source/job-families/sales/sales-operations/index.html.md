---
layout: job_family_page
title: "Sales Operations"
---

## Analyst

### Responsibilities
* Support the field sales team on opportunity management (creation, updating, closing)
* Collaborate with the sales and support functions to ensure SLAs are met
* Document and create knowledge repository for first-tier sales support
* Create and Implement reporting and dashboards for the sales organization that focuses on improving efficiency, effectiveness, and productivity
* Manage the operational aspect of on-boarding new employees and transitions of existing employees
* Identify opportunities for process automation and optimization, with a focus on scalability and driving significant growth
* Collaborate with Marketing to ensure proper lead management processes, metrics and policies
* Maintain data integrity within customer records in Salesforce.com and other systems
* Monitor system adoption and data compliance and governance
* Maintain and evangelize communication best practices for sales and sales support functions

### Requirements
* BA/BS degree
* Minimum 2 years relevant experience and an understanding of sales operations
* Strong analytical ability and able to prioritize multiple projects
* SFDC experience and knowledge of enterprise SaaS tools
* Excellent problem solving, project management, interpersonal and organizational skills
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)


## Manager

### Responsibilities

* Support the field sales team on quote processes, pricing, configuration, and terms
* Collaborate with the finance deal desk team to ensure that quote creation and approval SLAs are met
* Review non-standard deal terms and ensure compliance with published sales and approval policies and act as escalation point for approvals
* Provide first-tier support for any end-user technical or process questions in Salesforce and other systems primarily managed by Sales Operations
* Implement tools and processes for the sales organization that focus on improving efficiency, effectiveness, and productivity
* Identify opportunities for process automation and optimization, with a focus on scalability and driving significant growth
* Work with other departments to improve integration between Salesforce and other mission-critical systems, including Marketo, Zuora, and Zendesk
* Collaborate with Marketing to ensure proper lead management processes, metrics and policies
* Maintain data integrity within customer records in Salesforce.com and other systems
* Monitor system adoption and data compliance and governance
* Develop best practices that align sales data quality with company initiatives


### Requirements

* BA/BS degree
* Minimum 5 years relevant experience and solid understanding of sales operations
* Strong analytical ability and able to prioritize multiple projects
* Managed Deal Desk functions and an expert with CPQ tools
* Deep SFDC expertise and knowledge of enterprise SaaS tools
* Excellent problem solving, project management, interpersonal and organizational skills
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)


## Specialities
These business units under Sales will require an operations manager more familiar with their specific requirements:

## Customer Success

### Additional Responsibilities
* Build and optimize a company-wide customer success process in collaboration with the customer success team and broader GitLab organization
* Develop and implement lifecycle processes, policies and metrics to support customer outcomes and business growth, defining the appropriate touch points (i.e., human-led and digital) throughout the customer journey (pre and post initial sale)
* Develop methods, processes, systems and tools to appropriately support our customer segmentation models: enterprise, mid-market and SMB
* Drive cross-functional programs and initiatives to support scaling of the business and integrate the customer success engagement processes with teams throughout GitLab (e.g., sales, marketing, product)
* Lead the development and rollout of systems and digital tools to improve the team’s effectiveness and efficiency and provide increased customer insights through data, automation and telemetry
* Curate content and playbooks to allow the team to more efficiently deliver customer outcomes
* Create and coordinate key reporting for GitLab leadership team, partnering with the Customer Success analyst to provide reporting on customer adoption, sentiment and advocacy and business results (i.e., forecasts, renew, expand, churn)
* Act as liaison to customer success enablement to drive training and enablement priorities, content and methods

### Additional Requirements
* 3 years of customer success experience
* Experience with customer management systems (e.g., Salesforce, Gainsight/Totango/Client Success/etc., digital marketing tools)
* Experience with support and/or professional services a plus


## Director

Your mission is to organize data and generate deep customer insight in order to enhance sales force productivity and effectiveness. You will accomplish your mission by working collaboratively with our sales, marketing, finance and operations teams, you will measure everything to help us optimize our sales funnel and achieve growth. Your impact will help us make it easier to maintain a competitive edge, enhance sales force effectiveness, and achieve consistent, sustainable sales success.

### Responsibilities

* Partner with the VP Field Ops and sales leadership on all issues and challenges impacting the success of the sales team
* Lead, manage and evaluate the sales tools, processes, policies, and programs to ensure continuous productivity and effectiveness
* Partner with the Sales Strategy, Finance and HR to design, document, implement and monitor sales compensation plans
* Improve and manage the [Sales Handbook](/handbook/sales/)
* Create board-level presentations for the Chief Revenue Officer
* Manage, analyze, and summarize the weekly Sales Forecast
* Pricing and Contract Support: Given the pace of business, its imperative that sales operations enable the sales team with high-quality proposals that can be 
* Collaborate with regional sales leaders and teams to develop and execute sales management disciplines and processes (territory assignment & reviews, weekly/quarterly forecast, QBRs, pipeline analysis and development, account planning, account assignments, quota/budget allocation)

### Requirements

* 10-15 years of experience in a global SAAS sales or sales operations environment
* Ability to work independently with a high degree of accountability, while also able to collaborate cross-functionally (finance, marketing, sales enablement, etc) with exceptional intrapersonal skills
* Demonstrated passion for information and business intelligence; thorough understanding of sales processes and methodologies
* Minimum of 5 years experience in Salesforce.com
* Analytical and detail oriented, strong project management skills with a drive for results
* Exceptional written/verbal communication and presentation skills
* Proven ability to thrive in a fluid, fast-paced, unpredictable environment
* Interest in GitLab, and open source software
* Unquestionable ethics, integrity and business judgment; you share our values, and work in accordance with those values
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)


## Senior Director

Support the Field Sales organization by optimizing critical business processes, productivity tools, and ensuring consistently as we scale our WW Field Teams.  This requires collaboration with cross-functional departments and leveraging systems for related business processes.  The candidate must be a strong leader, extremely well organized, analytical, detailed-oriented, quality minded and help champion the Field Teams.

### Responsibilities

* Develop and execute a Global Sales Operations by collaborating with Sales Leadership and GTM teams including driving key initiatives 
* Develop and optimize sales processes and programs as it relates to Quote-to-Cash, Deal Desk, Forecasting and supporting Field Business Units
* Develop an operational cadence for Sales Operations 
* Proactively monitor and maintain high levels of quality, accuracy, and process consistency across the Field organization 
* Develop strategies to optimize SFDC, other SaaS tools and data across all business units
* Develops key performance metrics, dashboards and reports that help the sales organization focus on performance drivers and results 
* Collaborate with Marketing to ensure proper lead management processes, metrics and policies 
* Partner and implement knowledge management strategies to continuously increase product knowledge across all GTM teams 
* Be a trusted advisor to Sales Leadership 

### Requirements

* BA/BS degree
* 12+ years of relevant experience in a software business and sales operations capacity 
* Excellent quantitative analytical skills, creativity in problem solving, empowering teams and a keen business sense
* Ability to think strategically, but also have exceptional attention to detail to drive program management, execution and results 
* Demonstrated ability to influence and foster collaborative relationships with cross functional GTM departments
* Excellent problem solving, project management, interpersonal and organizational skills
* Deep SFDC expertise and expert knowledge of enterprise sales tools
* Ability to coach, motivate, performance manage and recruit Sales Operations teams
* Interest in GitLab, and open source software
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)
