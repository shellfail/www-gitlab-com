---
layout: job_family_page
title: "Financial Analyst"
---

We are looking to hire a Financial Analyst for our Financial Planning & Analysis team. The successful candidate will work directly with members of our senior leadership team to set plans, derive goals, and collaborate effectively with teams to achieve them. We’re looking for a detailed oriented self starter who can juggle multiple projects and is excited by building processes that scale.

###### Responsibilities:

- Plan the wholistic financial trajectory of the business. Work with senior leaders across functional groups to instrument the business. Determine key metrics and measure how resources will be deployed to achieve them.

- Oversee the company’s financial health and drive monthly reporting packages. Provide ongoing communication and collaboration with business partners. Surface the performance of our key financial and operational metrics to our senior leadership team and the Board.

- Embrace new information and proactively look for ways to better understand our business performance. Utilize new information to help improve visibility and forecasting.

###### Requirements:

- Bachelor in Accounting or Finance preferred; MBA a plus

- 3-5 years of relevant work experience, preferably at a leading investment bank or fast growing SaaS business.

- Significant experience with sophisticated financial modeling. Direct company FP&A experience is a plus.

- Ability to turn large data sets into meaningful insights. Ability to code using SQL is a plus.

- Solid understanding of the key drivers of a SaaS business.

- Proactive thinker looking to build processes that scale.

- Distill and simplify complex financial concepts for all audiences.

- Build trust and effectively collaborate cross functionally.
