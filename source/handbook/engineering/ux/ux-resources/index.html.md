---
layout: markdown_page
title: "UX Resources"
---

### On this page

{:.no_toc}

- TOC
{:toc}

# UX Resources

## Workflows

* [UX Department](/handbook/engineering/ux/ux-department-workflow)
* [Product Designer workflows](/handbook/engineering/ux/ux-designer)
* [UX Researcher workflows](/handbook/engineering/ux/ux-research)

## Design

### GitLab design project

The GitLab design project is primarily used by the Product Design team to host design files and hand them off for implementation. For details, please visit the project [README](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md).

* [View the project](https://gitlab.com/gitlab-org/gitlab-design/)
* [View spec sheets](https://gitlab-org.gitlab.io/gitlab-design/)

### Pajamas Design System

The GitLab Design System, [Pajamas][pajamas], was developed to increase iteration speed and bring consistency to the UI through reusable and robust components. This system helps keep the application [DRY](http://programmer.97things.oreilly.com/wiki/index.php/Don't_Repeat_Yourself) and allows designers to focus their efforts on solving user needs, rather than recreating elements and reinventing solutions. It also empowers Product, Engineering, and the Community to use these defined patterns when proposing solutions. It is currently a work in progress.

* [Visit design.gitlab.com](https://design.gitlab.com/)
* [View the project](https://gitlab.com/gitlab-org/design.gitlab.com)

### SVGs

Our SVG repository manages all GitLab SVG assets by creating an SVG sprite out of icons and optimizing SVG-based illustrations.

* [SVG Previewer](http://gitlab-org.gitlab.io/gitlab-svgs/)
* [View the project](https://gitlab.com/gitlab-org/gitlab-svgs)

### Frontend

Coming soon.

### Prototypes

Coming soon.

### Tools

Coming soon.

### Dribbble

GitLab has a Dribbble team account where you can add work in progress, coming soon, and recently released works.

* If a Dribbble post has a corresponding open issue, link to the issue so designers can contribute on GitLab.
* Add the Dribbble post to our [UX Design Twitter spreadsheet](https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit), along with a link to the corresponding open issue if applicable.
* If you’re not a member of the GitLab Dribbble team and would like to be, contact the UX Lead to grant you membership.
* [View the GitLab Dribbble team page](https://dribbble.com/GitLab)

## Research

### UX research project

The UX research project contains all research undertaken by GitLab's UX researchers and is only used for the organization and tracking of UX research issues.

* [View the project](https://gitlab.com/gitlab-org/ux-research)

### System usability score

Once each quarter, we run a [System Usability Scale (SUS)](https://www.usability.gov/how-to-and-tools/methods/system-usability-scale.html) survey to measure user perception of the GitLab product. We send the survey to members of the wider GitLab community, with the goal of asking for a response from any individual no more than twice per year.

* [Q1 FY20 results](/handbook/engineering/ux/sus/System-Usability-Scale-Q1FY2020.pdf)

### GitLab First Look

At GitLab, we want everyone to be able to contribute. To that end we created First Look where we accept applicants to participate in various studies and testing.

* [Visit GitLab First Look](https://about.gitlab.com/community/gitlab-first-look/index.html)

## UX design archive

The UX design archive is a collection of key design issues broken down by specific areas of GitLab. It is
not a comprehensive list. It is intended to shed insight into key UX design decisions.

* [Visit the UX design archive](/handbook/engineering/ux/design-archive)

## From the GitLab team

Not only do our team members create great work for the wider GitLab community, but they also create some amazing industry-related resources to push our craft forward.

* [Building Design Systems: Unify User Experiences through a Shared Design Language](https://www.amazon.com/Building-Design-Systems-Experiences-Language/dp/148424513X), by Taurie Davis and Sarrah Vesselov
* [Craft Awesome Web Typography](https://betterwebtype.com/web-typography-book/), by Matej Latin