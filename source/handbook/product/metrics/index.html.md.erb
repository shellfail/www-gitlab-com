---
layout: markdown_page
title: "Product Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Key Performance Indicators

Please note: Product KPIs are mapped 1 to 1 with our Growth teams in order to focus those teams on experiments to improve our KPIs. Additional metrics will be tracked and may add value, but should ultimately drive one or more KPI.

### Activation
Measured by the amount of users who completed a key onboarding step. This is defined as created a project, created or commented on an issue, or pushed a commit.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1427)

### Adoption
Measured by monthly growth in Stage Monthly Active Users (SMAU). A SMAU is a monthly active user (MAU) with stage-specific activity in GitLab in a calendar month.
Stage-to-activity mapping is defined [here](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/ping_metrics_to_stage_mapping_data.csv)

[Dashboard](https://app.periscopedata.com/app/gitlab/425984/Month-over-Month-Overestimated-SMAU) Note: this dashboard currently overestimates SMAU as it counts all users of the instance toward the SMAU if the stage was in use, and
does not adhere to our MAU definition.

### Upsell
Measured by the amount of users who moved from a paid tier to a higher paid tier in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1429)

### Retention
Measured by gross retention. Gross retention is defined as:
Gross Retention (%) = (min(B, A) / A) * 100%

A. MRR 12 months ago from currently active customers
B. Current MRR from the same set of customers as A.

[Dashboard](https://app.periscopedata.com/app/gitlab/412223/GitLab.com-Customer-Retention)

## Other Shared Metrics for GitLab.com and Self-Managed

### Monthly Active User (MAU)
A [user account](https://docs.gitlab.com/ee/user/profile) on a GitLab instance with at least 1
[Audit Event](https://docs.gitlab.com/ee/administration/audit_events.html) or 1 [Event](https://docs.gitlab.com/ee/api/events.html)
in a calendar month. Until June 2019, the definition of MAU for GitLab.com will include only
[Audit Events](https://docs.gitlab.com/ee/administration/audit_events.html).

[Dashboard](https://app.periscopedata.com/app/gitlab/425984/Month-over-Month-Overestimated-SMAU)

### Acquisition (New User)
Amount of new users who signed up for a GitLab account (GitLab.com or Self-Managed) in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1422)

### Expansion
Amount of paid groups that added users to the namespace in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1430)

### CI Pipeline Minute Consumption
Total number of CI Runner Minutes consumed in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1431)

### User Return Rate
Percent of users or groups that are still active between the current month and the prior month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1433)

### Churn
The opposite of User Return Rate. The percentage of users or groups that are no longer active in the current month, but were active in the prior month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1466)

### Projects
Number of existing [Projects](https://docs.gitlab.com/ee/user/project/) at a specified point in time. This number currently includes [Archived Projects](https://docs.gitlab.com/ee/user/project/settings/#archiving-a-project). 

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1456)

### New Projects
Number of new [Projects](https://about.gitlab.com/handbook/product/metrics/#projects) created in a calendar month. 

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1457)

### New Merge Requests
Number of [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/) created in a calendar month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1458)

### New Issues
Number of [Issues](https://docs.gitlab.com/ee/user/project/issues/) created in a calendar month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1459)

### Provably successful Direction items
This metric reports on the percentage of Direction items that have met or
exceeded their respective success metrics. For each feature labled ~Direction,
there should be a defined success metric, and telemetry configured to report
on that success metric to determine if it was provably successful.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1467)

## Other GitLab.com Only Metrics

### Conversion
Amount of users who moved from a free tier to a paid tier in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1428)

### Active Churned User
A GitLab.com user, who is not a MAU in month T, but was a MAU in month T-1.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1441)

### Active Retained User
A GitLab.com user, who is a MAU both in months T and T-1.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1442)

### New User
A newly registered GitLab.com user - no requirements on activity.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1443)

### Churned Returning User
A GitLab.com user, who is not a new user and who was not a MAU in month T-1, but is a MAU in month T.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1444)

### Paid User
A GitLab.com [Licensed User](https://about.gitlab.com/handbook/finance/operating-metrics/#licensed-users)

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1445)

### Paid MAU
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-user) [MAU](/handbook/finance/gitlabcom-metrics/index.html#monthly-active-user-mau).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1446)

### Active Retained Paid User
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-user) [Active Retained User](/handbook/finance/gitlabcom-metrics/index.html#active-retained-user)

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1447)

### Monthly Active Group (MAG)
A GitLab [Group](https://docs.gitlab.com/ee/user/group/), which contains at least 1 [project](https://docs.gitlab.com/ee/user/project/) since inception and has at least 1 [Event](https://docs.gitlab.com/ee/api/events.html) in a calendar month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1448)

### Active Churned Group
A GitLab.com group, which is not a MAG in month T, but was a MAG in month T-1.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1449)

### Active Retained Group
A GitLab.com group, which is a MAG both in months T and T-1.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1450)

### New Group
A newly created GitLab.com group - no requirements on activity.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1451)

### Paid Group
A GitLab.com group, which is part of a paid plan, i.e. Bronze, Silver or Gold. [Free licenses for Ultimate and Gold](/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/) are currently included.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1452)

### Paid MAG
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-group) [MAG](/handbook/finance/gitlabcom-metrics/index.html#monthly-active-group-mag).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1453)

### Active Retained Paid Group
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-group) [Active Retained Group](/handbook/finance/gitlabcom-metrics/index.html#active-retained-group)

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1454)

### Paid Group Member
A GitLab.com user, who is a member of a [Paid Group](https://about.gitlab.com/handbook/product/metrics/#paid-group).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1455)

### Paid CI pipeline minute consumption rate
The percent of users or groups that pay for additional CI pipeline minutes.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1432)

## Other Self-Managed Only Metrics

### Active Core Self Hosts
The count of active [Core Self Hosts](/pricing/#self-managed).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1460)

### Lost instances
A lost instance of self-managed GitLab didn't send a usage ping in the given month but it was active in the previous month

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1461)