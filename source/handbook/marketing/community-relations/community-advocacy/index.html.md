---
layout: markdown_page
title: "Community Advocacy"
---

## Finding the Community Advocates

- [**Community Advocacy Issue Tracker**](https://gitlab.com/gitlab-com/marketing/community-advocacy/general/issues); please use confidential issues for topics that should only be visible to team members at GitLab.
- [**Chat channel**](https://gitlab.slack.com/messages/community-relations); please use the `#community-relations` chat channel for questions that don't seem appropriate to use the issue tracker for.

## On this page
{:.no_toc}

- TOC
{:toc}

----

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Community Advocate Resources

- Community Advocate Onboarding
  - [Onboarding](/handbook/marketing/community-relations/community-advocacy/onboarding/checklist/)
- Community Advocate Bootcamp
  - [Bootcamp](/handbook/marketing/community-relations/community-advocacy/onboarding/bootcamp/)

----

## Role of Community Advocacy

### Goal

The goal of community advocacy is to grow the number of active GitLab content contributors. We do this by increasing conversion in the [contributor journey](/handbook/journeys/#contributor-journey).

### Plan

1. Have discount codes that are easily distributed by team members
1. Send every major contributor a personalized gift
1. Host online sessions for content contributors
1. Start keeping track of our core contributors
1. Do the rest of the [contributor journey](/handbook/journeys/#contributor-journey)

### Vision

1. GitLab has 1000's of active content contributors (e.g. for blogs, meetups, presentations, etc.)
1. Being a core contributor is a very rewarding experience
1. There are 10's of active GitLab/[ConvDev](http://conversationaldevelopment.com/) meet-ups
1. 100's of talks per year given at conferences and meetups
1. Our most active content contributors come to our summits
1. 100's of people contribute content about GitLab every month
1. We use software that helps us to keep track of core contributors (can be forum, Highrise, software made for advocacy, or a custom Rails app)
1. There is a core contributors page organized per region with the same information as the [team page](/company/team/) and what they contributed, where they work (if they have a linkedin profile), and a button to sent them an email via a form.
1. We measure and optimize every step of the [contributor journey](/handbook/journeys/#contributor-journey)

### Respond to every community question about GitLab asked online

- This includes helping members of the community with _their_ questions, but also making sure that the community is heard and that the feedback from the community reaches the rest of the team at GitLab.
- Engage with the developer community in a way that is direct but friendly and authentic. Be able to carry the tone of the GitLab brand while also giving the proper answers or direction to members of the community.
- [Engage with experts in the GitLab team](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts) to provide the best quality answers and to expose them to community feedback.
- Help update the [social media guidelines](/handbook/marketing/social-media-guidelines/) and GitLab voice as new situations arise.
- Explore different tools from Zendesk to Mentions to find a way to track all mentions of GitLab across the internet.
- Don’t be afraid of animated gifs and well-placed humor! We are not robots.
- Work within the GitLab process to help users report bugs, make feature requests, contact support, and provide feedback on the product.

### Social Response Time [KPI](/handbook/ceo/kpis/) Definition
Social Response Time is the time between an inbound message and the first-reply time.
The current goal is to be under 7 hours for all channels and under 5 hours for high-priority channels.
This response time is currently tracked in Zendesk.

#### Community response channels

The Community Advocates actively monitor and respond to the following set of channels.

In this overview:
- Those channels not marked as active need a response workflow to be put in place and are currently monitored on an occasional basis.
- Each channel has a link to the workflow in place to process responses

| CHANNEL | SOURCE | AUTOMATION | DESTINATION | ACTIVE? |
| - | - | - | - | - |
| [`@gitlab`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter) | Twitter mentions | Zendesk | Zendesk | ✓ |
| [`@movingtogitlab`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter)  | Twitter mentions | Zendesk | Tweetdeck | ✓ |
| [`@gitlabstatus`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter)  | Twitter mentions | Zendesk | Zendesk | ✓ |
| [Facebook](/handbook/marketing/community-relations/community-advocacy/workflows/facebook)  | Facebook page messages | Zapier | Zendesk | ✓ |
| [Hacker News](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews) | Hacker News mentions | Zapier | Zendesk and Slack: #hn-mentions | ✓ |
| [Hacker News front page stories](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews) | Hacker News front page mentions | Zapier | Slack: #community-advocates | ✓ |
| [Education initiative](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource) | Education application form | Marketo | Salesforce and Zendesk | ✓ |
| [Open Source initiative](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource) | Open Source application form | Marketo | Salesforce and Zendesk | ✓ |
| [E-mail (merch@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Shop contact | E-mail alias | Zendesk | ✓ |
| [E-mail (community@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Handbook | E-mail alias | Zendesk | ✓ |
| [E-mail (movingtogitlab@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | #movingtogitlab campaign (deprecated) | E-mail alias | Zendesk | ✓ |
| [E-mail (education@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (opensource@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (personal inbox)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail) | E-mails to track as tickets | E-mail alias | Zendesk | ✓ |
| [Website: blog](/handbook/marketing/community-relations/community-advocacy/workflows/blog) | Disqus comments | Zapier | Zendesk, #mentions-of-gitlab | ✓ |
| [Website: DevOps Tools](/handbook/marketing/community-relations/community-advocacy/workflows/devops-tools) | Disqus comments | Zapier | Zendesk and Slack: #devops-tools-comments | ✓ |
| [Speakers](/handbook/marketing/community-relations/evangelist-program/workflows/find-a-speaker) | Find-a-speaker form | Zapier | Zendesk | ✓ |
| [Reddit](/handbook/marketing/community-relations/community-advocacy/workflows/reddit)  | Reddit mentions | Zapier | Zendesk and Slack: #reddit | ✓ |
| [Documentation](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | Disqus comments | Zapier | Slack: #docs-comments | ✓ (Docs Team)|
| [Stack Overflow](/handbook/marketing/community-relations/community-advocacy/workflows/stackoverflow) | Stack Exchange mentions | Zapier | Zendesk | ✓ |
| [GitLab forum](/handbook/marketing/community-relations/community-advocacy/workflows/forum) | forum.gitlab.com | Zapier | Zendesk and Slack: #gitlab-forum | ✓ |
| [Lobste.rs](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | lobste.rs mentions | Zapier | Slack: #mentions-of-gitlab | ✖ |
| [IRC](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | IRC support | N/A | N/A | ✖ |
| [Gitter](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | Gitter support | N/A | N/A | ✖ |
| [YouTube](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | YouTube comments | N/A | N/A | ✖ |
| [Mailing list](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | GitLabHq Google Group (deprecated) | N/A | N/A | ✖ |
| [Quora](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | GitLab Quora topic | N/A | N/A | ✖ |
| [Wider community content](/handbook/marketing/community-relations/community-advocacy/workflows/inactive) | Blog post comments | N/A | N/A | ✖ |

## How we work

- [Community advocacy workflows](/handbook/marketing/community-relations/community-advocacy/workflows/)
- [Community advocacy guidelines](/handbook/marketing/community-relations/community-advocacy/guidelines/)

## Deliverable scheduling

* Team meetings are on Tuesday
* One on one's are on Thursday
* All deliverables are expected to be completed by Friday of the running week
  * Lots of time to complete
  * Enough time to review
  * Enough time for resolving potential problems
  * Small deliverables force small / fast iterations
* All deliverables are expected to be merged by Tuesday
* For every handbook update (that substantially change content/layout), please follow the [handbook guidelines](/handbook/handbook-usage/#handbook-guidelines)

## Release day advocate duty

Every 22nd of the month we release a new version of GitLab. More often than not we get a spike in community mentions. To help deal with this we have dedicated release advocates that own the effort of responding to community mentions on/after a release.

Every month a different advocate has release advocate duty. It rotates on a monthly basis. If the release day takes place on a weekend, one of the advocates is assigned to monitor the traffic and to process mentions. We keep track of the assignments on the `Community Advocates` GitLab team calendar.

The two channels that we see the biggest increases in are:

* The GitLab blog
* HackerNews

### Release day tasks

- Consider engaging with an [Advocate-for-a-day](#advocate-for-a-day) in advance
- Monitor the `#release-post` Slack channel throughout the day to be ready at the time the release blog post is published
- Copy the overview of the three main features or improvements from the beginning of the release blog post. Post this overview on the relevant social channels (HackerNews will be the main one to post it to). You can use the [11.8 summary post](https://news.ycombinator.com/item?id=19228781) as an example.

## Involving experts

As Community Advocates, we will often want to involve experts in a topic being discussed online. The [Involving experts workflow section](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts) describes how we do it.

### Can you please respond to this?

You got a link to this because we'd like you to respond to the mentioned community comment. We want to make sure we give the best answer possible by connecting the wider community with our experts and expose you to more community feedback.

When responding to community mentions, you should check out the [social media guidelines](/handbook/marketing/social-media-guidelines/). Please answer in the social channel that the comment was originally posted in - discussing it internally via Slack makes it impossible for the community member to interact.

If you can't respond to the linked comment, that's OK, but please quickly let the person who pinged you know so they can ping someone else.

## Initiatives

### Education / OSS

While we're restructuring our handbook, this topic has now moved to the [Education/open Source workflow section](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource).

### Supporting community initiatives

When we see outstanding articles about GitLab from our wider community, we should acknowledge the author.

Please include the link to that article in [this Google Doc](https://docs.google.com/document/d/1fqiX3Z6F3AHuMKKmyqK5wWrtKnw0OH_5Ez9Q6Np2xZE/edit). Our evangelist team will proceed from there.

### #movingtogitlab

During news cycles such as the [Microsoft acquisition of GitHub](https://news.microsoft.com/2018/06/04/microsoft-to-acquire-github-for-7-5-billion/), there may be a an increase in new GitLab users. If so, advocates may need to reinitiate the @movingtogitlab Twitter account and/or respond to the Twitter hashtag #movingtogitlab to welcome these accounts. The [#movingtogitlab](/handbook/marketing/community-relations/community-advocacy/workflows/moving-to-gitlab) workflow list some example responses and etiquette.

## Advocate for a Day

When community advocates aren't available, or we expect high traffic on social media (because of some major outage, or some significant announcement), we should try to recruit more GitLab team-members who would help us cover our social networks. Our [Advocate for a Day](/handbook/marketing/community-relations/community-advocacy/workflows/advocate-for-a-day) page is meant to help assist anyone who has been asked to perform this duty.

## Expertises

Every Community Advocate owns one or more of the processes that the CA team uses. These are called expertises.

* Swag, Supporting Community Individuals - [Djordje](https://gitlab.com/sumenkovic)
* Initiatives, Maintainer of [OSS project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program) - [Borivoje](https://gitlab.com/borivoje)

## Relevant Links

- [Social Media Guidelines](/handbook/marketing/social-media-guidelines/)
- [Support handbook](/handbook/support/)
